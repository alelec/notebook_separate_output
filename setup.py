import setuptools

setuptools.setup(
    name="notebook_separate_output",
    packages=['notebook_separate_output'],
    author='Andrew Leech',
    author_email='andrew.leech@planetinnovation.com.au',
    description='Jupyter Notebook extension to redirect cell output to separate text file.',
    url='https://gitlab.com/alelec/notebook_separate_output',
    setup_requires=['setuptools_scm'],
    use_scm_version=True,  # Generate version number from git commit/tags

    # include_package_data=True,
    # data_files=[
    #     # like `jupyter nbextension install --sys-prefix`
    #     ("share/jupyter/nbextensions/my_fancy_module", [
    #         "my_fancy_module/static/index.js",
    #     ]),
    #     # like `jupyter nbextension enable --sys-prefix`
    #     ("etc/jupyter/nbconfig/notebook.d", [
    #         "jupyter-config/nbconfig/notebook.d/my_fancy_module.json"
    #     ]),
    #     # like `jupyter serverextension enable --sys-prefix`
    #     ("etc/jupyter/jupyter_notebook_config.d", [
    #         "jupyter-config/jupyter_notebook_config.d/my_fancy_module.json"
    #     ])
    # ],
    # zip_safe=False
)
