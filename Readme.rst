notebook separate output
========================

Jupyter Notebook extension to redirect cell output to separate text file.
This prevents the cell output text from being saved to the ipynb, helping to keep them cleaner for committing to git etc.

Output will be saved to a `*.log` file next to the original ipynb file

The log file will be overwritten on each save with the current cell inputs/outputs.

With this extension installed, the outputs will always be blank when the ipynb is reloaded / notebook restarted.

To install::

    pip install notebook_separate_output
    jupyter serverextension enable --py jupyter_separate_output --sys-prefix
